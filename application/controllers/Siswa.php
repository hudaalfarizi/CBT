<?php if(!defined('BASEPATH')) exit('No direct scripts access allowed');

/**
 * @author : Huda Alfarizi
 * @version : 1.1
 * @since : 15 Mei 2018
 */
class Siswa extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    if($this->session->userdata('status') != "login") { redirect('Login') ;};
    $this->load->model('Soal_model');
  }

  function index(){
  $data['judul'] = 'CBT';
  $data['main_content'] = 'admin';
  $this->load->view('includes/template',$data);
 }
 function pilih_ujian(){
  $id = $this->session->userdata('id');
    $p_kelas = $this->Soal_model->get_kelas($id)->result();
    $kelas = array();
    foreach($p_kelas as $row){
      $kelas['no'] = $row->Kelas;
    }
    $sk = $kelas['no'];
    $data['pm'] = $this->Soal_model->get_mapel($sk)->result();
    $data['judul'] = 'Pilih Mata Pelajaran';
    $data['main_content'] = "pilih_ujian";
    $this->load->view('includes/template',$data);

 }
 function ujian(){
    $mp = $this->input->post('pm');
    $id = $this->session->userdata('id');
    $p_kelas = $this->Soal_model->get_kelas($id)->result();
    $kelas = array();
    foreach($p_kelas as $row){
      $kelas['no'] = $row->Kelas;
    }
    $sk = $kelas['no'];
    $data['soal'] = $this->Soal_model->get_soal($sk,$mp)->result();
    $data['jumlah'] = $this->Soal_model->get_soal($sk,$mp)->num_rows();
    $data['judul'] = 'Ujian Online';
    $data['main_content'] = 'ujian';
    $data['mata_pelajaran'] = $mp;
    $data['Kelas'] = $sk;
   $this->load->view('includes/template',$data);
 }
 function jawab(){
  date_default_timezone_set('Asia/Bangkok');
  $benar = 0;
  $id_soal = $this->input->post('id');
  $pilihan = $this->input->post('pilihan');
  $jumlah = $this->input->post('jumlah');
  for ($i=0;$i<$jumlah;$i++){
  $nomor = $id_soal[$i];
  $jawaban = $pilihan[$nomor];
  $cek = $this->Soal_model->cek_jawaban($nomor,$jawaban)->num_rows();
  if($cek){$benar++;}

 }
 $score = 100/$jumlah*$benar;
 $data['judul'] = 'Hasil Ujian';
 $data['main_content'] = 'hasil_ujian';
 $data['score'] = $score;
 $data['benar'] = $benar;
 $insert = array("IdSiswa" => $this->session->userdata('id'),"IdMapel" => $this->input->post('mata_pelajaran'), "Nilai" => $score , "Tanggal" => date('Y-m-d'));
 $this->db->insert("nilai",$insert);
 $this->load->view('includes/template',$data);
}
function Logout(){
  $this->session->sess_destroy();
  redirect('Login');
}
}