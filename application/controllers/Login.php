<?php

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('M_login');

	}

	function index(){
		$this->load->view('login');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => sha1($password),
			);
		$cek = $this->M_login->cek_login("member",$where)->num_rows();
		$data = $this->M_login->cek_login("member",$where)->result();
		if($cek > 0){
			foreach($data as $row){
			$data_session = array(
				'id' => $row->IdMember,
				'nama' => $row->Nama,
				'username' => $row->Username,
				'level' => $row->Level,
				'status' => "login"
				);
		}
			$this->session->set_userdata($data_session);
			if($this->session->userdata['level'] = 'siswa'){
			redirect('Siswa'); } else { redirect('Admin') ;}

		}else{
			echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
