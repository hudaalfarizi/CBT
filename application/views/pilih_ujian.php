<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user" aria-hidden="true"></i> <?php echo $this->session->userdata('nama');?>
        <small>Pilih mata pelajaran</small>
      </h1>
    </section>
<?php echo form_open('Siswa/ujian');?>
    <section class="content">
        <div class="col-md-12" style="background-color: white;">
		<select class="form-control" name="pm">
            <option>...</option>
            <?php foreach($pm as $row){ ?>
            <option value="<?php echo $row->IdMapel;?>"><?php echo $row->Mapel;?> </option>
            <?php  } ?>
        </select>
	</div>
    </section>
    <br>
    <button class="btn btn-mini btn-info" type="submit"> Simpan</button>
    <?php echo form_close();?>
</div>