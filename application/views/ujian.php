<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user" aria-hidden="true"></i> <?php echo $this->session->userdata('nama');?>
        <small></small>
      </h1>
    </section>

    <section class="content">
        <?php echo form_open('Siswa/jawab');?>
        <div class="col-md-12" style="background-color: white;">
		<?php $no=1; foreach ($soal as $row): ?>
			<?php echo $no;?>
			<?php echo $row->Soal ;
            $Id = $row->IdSoal;?><br>
            <input hidden="hidden" name="mata_pelajaran" value="<?php echo $mata_pelajaran;?>">
            <input hidden="hidden" name="kelas" value="<?php echo $Kelas;?>">
            <input hidden="hidden" name="id[]" value="<?php echo $row->IdSoal;?>">
            <input type="hidden" name="jumlah" value=<?php echo $jumlah; ?>>
            <input value="a" type="radio" name="pilihan[<?php echo $Id; ?>]"><?php echo $row->pilihan_a ?><br>
            <input value="b" type="radio" name="pilihan[<?php echo $Id; ?>]"><?php echo $row->pilihan_b ?><br>
            <input value="c" type="radio" name="pilihan[<?php echo $Id; ?>]"><?php echo $row->pilihan_c ?><br>
            <input value="d" type="radio" name="pilihan[<?php echo $Id; ?>]"><?php echo $row->pilihan_d ?><br>
            <input value="e" type="radio" name="pilihan[<?php echo $Id; ?>]"><?php echo $row->pilihan_e ?><br>
		<?php $no++; endforeach ?>
	</div>
    <button type="submit" class="btn btn-mini btn-success"> SELESAI</button>
    <?php echo form_close();?>
    </section>
</div>